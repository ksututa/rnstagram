Это проект, над которым мы будем работать в течение курса

Урок 1

Презентации:
https://docs.google.com/presentation/d/1zR8iemhmew212jAAOZEGwSc-98s5H-kBXfL-oZNeXOw/edit?usp=sharing
https://docs.google.com/presentation/d/1drOFbzRmEVCqBomDDvBE_LzfNlfK_lAumPeSjxLUmMg/edit?usp=sharing

Видео:
https://youtu.be/bvqXpyterRQ

Домашнее задание:
1. Задача по вёрстке https://vk.com/photo28943318_456241771
2. Настроить окружение

Полезные материалы:
1. Getting Started https://facebook.github.io/react-native/docs/getting-started.html
2. Пустой Expo project https://vk.com/doc28943318_489756113?hash=477792a2b49b7a66a3&dl=99c9f6b61fccd34761

-------------------

Урок 2

Презентация:
https://docs.google.com/presentation/d/1BoQoSm1GmQ0AqF-muSsntgL5n113OEEjA7daJ3Begbw/edit?usp=sharing

Видео:
https://youtu.be/Vvv8vueDu-U

Домашнее задание:
1. Финализировать настройки окружения, чтобы нам больше не приходилось к этому возвращаться.
Не важно expo или native code, эмулятор или физическое устройство. Главное чтобы запускалось и работало.
2. Зарегистрироваться на gitlab, создать новый проект RNstagram, следуя инструкциям Existing folder подключить свой проект к гиту и выложить.
В Settings -> Members добавить меня (ksututa) в свой проект.
3. Создать компонент-функцию (без своего состояния) Author, который будет принимать ник и ссылку на фото
(например вот https://s3.amazonaws.com/uifaces/faces/twitter/tjrus/128.jpg),
а рисовать вот такой блок: https://vk.com/photo28943318_456241801 (выделено красным).
Высота в rem'ах, ширина должна растягиваться на максимум, фото круглое, размеры фото в rem'ах. Про компонент Image почитайте сами
4. Создать компонент PostImage, который принимает ссылку на картинку (например http://lorempixel.com/640/480/cats),
а рисует её обрезанной 1х1 в полную ширину экрана
5. Из компонента Post убрать текст и CustomInput и вставить Author и PostImage.
Должно быть максимально похоже на инстаграм-пост, только без комментариев и лайков.
Готовую работу закоммитить и запушить в свой репозиторий

Полезные материалы:
1. https://gitlab.com
2. Функция расчета rem
const rem = (side) => {
    return side > 310
        ? side > 350
            ? side > 390
                ? side > 470
                    ? side > 530
                        ? side > 590
                            ? side > 790
                                ? side > 990
                                    ? 36
                                : 30
                            : 22
                        : 22
                    : 22
                : 22
            : 15
        : 15
    : 12;
};

---------------------------------

Урок 3

Презентация:
https://docs.google.com/presentation/d/130HHikVLWyFl2lizAa0iR_1I9mzv_M3jbW1OMkP7Zes/edit?usp=sharing

Видео:
https://youtu.be/HR627Ybw-LI

Домашнее задание:
1. Сделать шапку RNstagram с нашим новым шрифтом (Billabong.ttf). Это должен быть отдельный компонент Header. Вставить его в App.
2. Создать компонент Feed, в котором потом будет находиться наша лента новостей, его так же вставить в App.
3. Feed должен импортировать post.js из lib и передавать его в компонент Post.
4. Post принимает this.props.post и отображает следующую информацию: автора с именем и аватаром, фото, иконки "лайк" и "комментарий".
5. Создать компонент Comment, который принимает комментарий и отображает имя автора и текст. Вставить его в компонент Post и передать первый комментарий из массива post.comments.
Запушить в свой репозиторий не позднее 18:00 в пятницу

Полезные материалы:
1. Иконки project with native code https://github.com/oblador/react-native-vector-icons
2. Иконки expo https://docs.expo.io/versions/v32.0.0/guides/icons/
3. Шрифты project with native code https://medium.com/react-native-training/react-native-custom-fonts-ccc9aacf9e5e
4. Шрифты expo https://docs.expo.io/versions/latest/guides/using-custom-fonts/
5. Жизненные циклы: https://reactjs.org/docs/react-component.html

-----------------------

Урок 4

Презентация:
https://docs.google.com/presentation/d/1kDelI5iRdoEed4UfXR9bqO5Yi3faNwLTEgewkMijqfU/edit?usp=sharing

Видео:
Исходники: https://drive.google.com/drive/folders/1rZw6egUi1pmz_wl64-jY6QPb7xs9IJF6?usp=sharing

Домашнее задание:
1. Тем у кого экспо: замените url localhost:3000 на http://my-json-server.typicode.com/petrovakd/rnstagram там правда ограничение на объем данных, так что всего десять постов и по одному комментарию. я подумаю как сделать мощнее.
2. Сверстать  TabBar c тремя кнопками "домой", "плюс", "пользователь" (использовать иконки). Это должны быть компоненты TouchableOpacity, но пока без функции onPress. Вставить TabBar вниз в App
3. *Доработать ленту по своему усмотрению - например выводить лайки, сделать счетчик лайков внутри поста, показывать аватары и лайки в комментариях

Полезные материалы:
1. json-server https://github.com/typicode/json-server
2. axios https://github.com/axios/axios
3. JSON.stringify() https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify 
4. Array.map() https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/map
5. Деструктуризация https://learn.javascript.ru/destructuring#destrukturizatsiya-obekta
6. ScrollView https://facebook.github.io/react-native/docs/scrollview
7. FlatList https://facebook.github.io/react-native/docs/flatlist
8. Шаблонные строки https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/template_strings
9. Base64
a.     https://developer.mozilla.org/ru/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
b.     https://facebook.github.io/react-native/docs/images


----------------------

Урок 5

Презентация:
https://docs.google.com/presentation/d/1PjUwc7V-LINGnLI1FZHemsSq_kgviI91N2NOT74EI7I/edit?usp=sharing

Видео:
https://youtu.be/WzaQ_0GD6iY

Домашнее задание:
1. Настроить header в AppStack, чтобы у него были кнопки "Отмена" и "Далее" на роуте PhotoPicker и "Назад" и "Поделиться" на роуте PostInfo
2. Сделать пагинацию в картинках (жеательно подгрузка по скроллу, если не получится - то загружать, пока не вытащите 100 фото например) 
3. Настроить tabBar в PhotoPicker, там должны быть лейблы без иконок "Бибилиотека" и "Фото"
4. Отобразить ситуацию, когда пользователь не дал разрешение для доступа к фото и когда галерея пользователя пустая

Полезные материалы:
1. Настройки StackNavigator https://reactnavigation.org/docs/en/stack-navigator.html
2. Настройки TabNavigator https://reactnavigation.org/docs/en/bottom-tab-navigator.html
3. BackHandler android https://facebook.github.io/react-native/docs/backhandler
4. CameraRoll https://facebook.github.io/react-native/docs/cameraroll
5. PermissionsAndroid https://facebook.github.io/react-native/docs/permissionsandroid
6. Permissions (expo) https://docs.expo.io/versions/latest/sdk/permissions

---------------

Урок 6

Презентация:
https://docs.google.com/presentation/d/1MwfMYe860tgmcNosUXy7LuiCt7vHlcS17I7Ki7hCYJ8/edit?usp=sharing

Видео:
https://drive.google.com/file/d/1TuaZpbwoCCiSZCNeSudtzO1yNNp65zlu/view

Домашнее задание:
1. Сделать авторизацию более похожей на Инстаграм, использовать KeyboardAwareScrollView в компоненте Auth при необходимости
2. Сверстать Splash
3. Валидировать поля при авторизации
4. Отображать ошибки регистрации и авторизации
5. Помимо токена, сохранить в сессии так же email пользователя

Полезные материалы:
1. Библиотека для авторизации https://github.com/jeremyben/json-server-auth
2. KeyboardAwareScrollView https://github.com/APSL/react-native-keyboard-aware-scroll-view
3. Хранение сессии https://facebook.github.io/react-native/docs/asyncstorage
4. Ограничение доступа к роутам https://github.com/jeremyben/json-server-auth#guarded-routes-
5. Заголовки https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
6. Bearer authorization https://swagger.io/docs/specification/authentication/bearer-authentication/
7. Камера PwNC https://github.com/react-native-community/react-native-camera
8. Камера expo https://docs.expo.io/versions/latest/sdk/camera/
9. Refs https://reactnatve.wordpress.com/2016/05/24/refs-to-components/
10. Жизненные циклы в навигации https://reactnavigation.org/docs/en/navigation-lifecycle.html
11. Ошибка Camera not running https://github.com/react-native-community/react-native-camera/issues/1626

---------------------

Урок 7

Презентация:
https://docs.google.com/presentation/d/1LwOf6pSKHZ7gFDN5a35kmuMZ_MCn7IA-wEhwV-BDk24/edit?usp=sharing

Видео:


Домашнее задание:
1. Вернуть Feed в рабочее состояние (отображаем все посты с пагинацией)
2. На экране пользователя загружаем только свои посты и отображаем плиткой
3. В настройках сделать возможность выбрать аватар и сохранить его в сессии
4. В шапке экрана пользователя отобразить аватар и имя (почту) пользователя
5. Добавить аватар при отправке поста на сервер

Полезные материалы:
1. Доступ к файловой системе https://github.com/itinance/react-native-fs
2. Доступ к галерее в экспо https://docs.expo.io/versions/latest/sdk/imagepicker/
3. Слушатель событий фокусировки при навигации https://reactnavigation.org/docs/en/navigation-prop.html#addlistener-subscribe-to-updates-to-navigation-lifecycle
4. Передача данных с помощью screenProps https://reactnavigation.org/docs/en/stack-navigator.html#navigator-props
5. Отображение фото в base64 https://facebook.github.io/react-native/docs/images#uri-data-images
6. Drawer-навигатор https://reactnavigation.org/docs/en/drawer-based-navigation.html
7. Анимация
https://facebook.github.io/react-native/docs/animations
https://facebook.github.io/react-native/docs/animated
https://code.tutsplus.com/ru/tutorials/practical-animations-in-react-native--cms-27567





