import React from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  AsyncStorage,
  Animated,
  Vibration,
} from 'react-native';
import axios from 'axios';
import c from '../../lib/const';

export default class Auth extends React.Component {
  constructor() {
    super();
    this.state = {
      email: 'example@mail.com',
      password: 'password',
      error: ''
    }
    this.navigateToRegister = this.navigateToRegister.bind(this);
    this.setEmail = this.setEmail.bind(this);
    this.setPassword = this.setPassword.bind(this);
    this.animate = this.animate.bind(this);
    this.animatedValue = new Animated.Value(0);
  }
  setEmail(email) {
    this.setState({ email });
  }
  setPassword(password) {
    this.setState({ password })
  }
  componentDidMount() {
    this.animatedValue.setValue(0);
  }
  async saveSession(token, email) {
    // console.log(userInfo);
    console.log(token, email);
    try {
      await AsyncStorage.setItem('userToken', token)
      await AsyncStorage.setItem('userEmail', email)
    } catch (e) {
      console.log(e);
    }
  }
  animate() {
    this.animatedValue.setValue(0);
    Vibration.vibrate(500);
    Animated.timing(this.animatedValue, {
      toValue: 8,
      duration: 500,
    }).start();
  }
  async login(email, password) {
    try {
      console.log(email, password);
      const url = 'http://10.0.2.2:3000/login'
      const response = await axios.post(url, {
        email,
        password,
      });
      console.log(response);
      if (response.data.accessToken) {
        // alert(response.data.accessToken);
        this.saveSession(response.data.accessToken, email);
        this.props.navigation.navigate('AppStack');
      } else {
        throw response;
      }
    } catch (e) {
      if (e.response.status === 400) {
        this.setState({ error: 'Неверное имя пользователя или пароль' })
      } else {
        this.setState({ error: JSON.stringify(e) })
      }
      console.log(e);
      this.animate();
    }
  }
  navigateToRegister() {
    this.props.navigation.navigate('Register');
  }
  render() {
    const animatedStyle = {
      transform: [{
        translateX: this.animatedValue.interpolate({
          inputRange: [0, 1, 2, 3, 4, 5, 6, 7, 8],
          outputRange: [0, -20, 0, 20, 0, -20, 0, 20, 0],
        })
      }]
    }
    const error = this.state.error ?
      <Animated.View style={[styles.errorArea, animatedStyle]}>
        <Text style={styles.errorText}>{this.state.error}</Text>
      </Animated.View> : <View style={styles.errorArea} />;
    console.log(this.state.email, this.state.password);
    return (
      <View style={styles.container}>
        <Text>Авторизация</Text>
        <TextInput
          value={this.state.email}
          style={styles.input}
          onChangeText={this.setEmail}
          placeholder={'Адрес электронной почты'}
        />
        <TextInput
          value={this.state.password}
          style={styles.input}
          onChangeText={this.setPassword}
          placeholder={'Пароль'}
        />
        {error}
        <Button
          onPress={() => this.login(this.state.email, this.state.password)}
          title={'Войти'} />
        <TouchableOpacity
          onPress={this.navigateToRegister}
        >
          <Text>Нет аккаунта? Зарегистрироваться</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 3 * c.rem,
  },
  input: {
    height: 3 * c.rem,
    width: '80%',
    borderColor: c.GREY,
    borderWidth: 1,
    margin: c.rem,
  },
  errorArea: {
    height: 2 * c.rem,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorText: {
    color: 'red'
  },
});