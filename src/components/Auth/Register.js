import React from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import c from '../../lib/const';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      email: 'example@mail.com',
      password: 'password',
      repeatPassword: 'password',
      loading: false,
      error: '',
    }
    this.navigateToAuth = this.navigateToAuth.bind(this);
    this.setEmail = this.setEmail.bind(this);
    this.setPassword = this.setPassword.bind(this);
    this.setRepeatPassword = this.setRepeatPassword.bind(this);
  }
  setEmail(email) {
    this.setState({ email });
  }
  setPassword(password) {
    this.setState({ password })
  }
  setRepeatPassword(repeatPassword) {
    this.setState({ repeatPassword })
  }
  async register(email, password, repeatPassword) {
    try {
      this.setState({ loading: true, error: '' })
      if (!email) {
        throw 'Адрес электронной почты не должен быть пустым'
      }
      if (!password) {
        throw 'Пароль не должен быть пустым'
      }
      if (password !== repeatPassword) {
        throw 'Пароли не совпадают'
      }
      console.log(email, password);
      const url = 'http://10.0.2.2:3000/users'
      const response = await axios.post(url, {
        email,
        password,
      });
      console.log(response);
      if (response.data.accessToken) {
        this.setState({ loading: false })
        this.props.navigation.navigate('Auth');
      } else {
        throw 'Ошибка запроса';
      }
    } catch (e) {
      this.setState({ loading: false, error: e })
    }
  }
  navigateToAuth() {
    this.props.navigation.navigate('Auth');
  }
  render() {
    console.log(this.state.email, this.state.password);
    return (
      <KeyboardAwareScrollView
        enableOnAndroid={true}
      >
        <View style={styles.container}>
          <Text>Регистрация</Text>
          <Text style={{ color: 'red' }}>
            {this.state.error}
          </Text>
          <TextInput
            value={this.state.email}
            style={styles.input}
            onChangeText={this.setEmail}
            placeholder={'Адрес электронной почты'}
          />
          <TextInput
            value={this.state.password}
            style={styles.input}
            onChangeText={this.setPassword}
            placeholder={'Пароль'}
            secureTextEntry={true}
          />
          <TextInput
            value={this.state.repeatPassword}
            style={styles.input}
            onChangeText={this.setRepeatPassword}
            placeholder={'Повторите пароль'}
            secureTextEntry={true}
          />
          <Button
            onPress={() => this.register(
              this.state.email,
              this.state.password,
              this.state.repeatPassword,
            )}
            title={'Зарегистрироваться'} />
            <TouchableOpacity
           onPress={this.navigateToAuth}>
          <Text>Уже есть аккаунт? Войти</Text>
        </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 3 * c.rem,
  },
  input: {
    height: 3 * c.rem,
    width: '80%',
    borderColor: c.GREY,
    borderWidth: 1,
    margin: c.rem,
  },
});