import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage
} from 'react-native';
export default class Splash extends React.Component {
  constructor() {
    super();
    this.getSession();
  }
  async getSession() {
    console.log('getSession');
    const token = await AsyncStorage.getItem('userToken');
    // alert(response)
    this.props.navigation.navigate(token ? 'AppStack' : 'AuthStack')
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Splash</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});