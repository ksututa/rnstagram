import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Button,
  AsyncStorage
} from 'react-native';
import c from '../../lib/const';
import axios from 'axios';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class PostInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      comment: '',
    }
    this.onShare = this.onShare.bind(this)
    this.setComment = this.setComment.bind(this);
  }
  async onShare() {
    const token = await AsyncStorage.getItem('userToken');
    const email = await AsyncStorage.getItem('userEmail');
    const user = {
      name: email,
        id: email,
        avatar: '',
    };
    const post = {
      user,
      photo: this.props.screenProps.photo,
      comments: [],
      // comments: this.state.comment ? [{ user, text: this.state.comment}] : [],
    }
    if (this.state.comment) {
      post.comments.push({
        user,
        text: this.state.comment,
      })
    }
    const url = 'http://10.0.2.2:3000/660/posts';
    try {
      const response = await axios.post(url, post, {
        headers: {
          Authorization: 'Bearer ' + token
        }
      })
      console.log(response);
      if (response.status === 201) {
        this.props.navigation.navigate('Feed');
      }
    } catch (e) {
      console.log(e);
    } 
  }
  setComment(comment) {
    this.setState({ comment })
  }
  render() {
    console.log(this.props.screenProps, this.props.screenProps.photo);
    return (
      <KeyboardAwareScrollView enableOnAndroid={true}>
        <View style={styles.container}>
          <Image style={styles.image}
            source={{ uri: this.props.screenProps.photo }}
          />
          <TextInput
            multiline={true}
            style={styles.input}
            value={this.state.comment}
            onChangeText={this.setComment}
          />
          <Button
            title={'Поделиться'}
            onPress={this.onShare}
            />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: c.WIDTH / 4 * 3,
    width: c.WIDTH / 4 * 3,
  },
  input: {
    width: c.WIDTH / 4 * 3,
    height: 5 * c.rem,
    borderColor: c.GREY,
    borderWidth: 1,
    borderRadius: 0.5 * c.rem,
    padding: 0.5 * c.rem,
    marginTop: c.rem,
    textAlignVertical: 'top',
  }
});