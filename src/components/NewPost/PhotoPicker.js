import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  CameraRoll,
  Image,
  ScrollView,
  PermissionsAndroid, // for projects with native code
} from 'react-native';
// import { Permissions } from 'expo'; // for expo only
import c from '../../lib/const';
import RNFS from 'react-native-fs';

export default class PhotoPicker extends React.Component {
  constructor() {
    super();
    this.state = {
      photos: [],
      selectedPhotoUri: '',
    }
  }
  async componentDidMount() {
    // await Permissions.askAsync(Permissions.CAMERA_ROLL);
    // expo variant
    await this.getStoragePermission();
    // pwnc variant
    await this.getPhotos();
  }
  async getStoragePermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
      )
      console.log(granted);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('can use camera');
      }
    } catch (e) {
      console.log(e);
    }
  } // for project with native code
  async getPhotos() {
    try {
      // индикатор загрузки
      const result = await CameraRoll.getPhotos({
        first: 17,
      })
      if (result.edges.length) {
        this.setState({
          photos: result.edges,
        })
        this.selectPhoto(result.edges[0])
      }
      console.log(result);
    } catch (e) {
      // выводить ошибку
      console.log(e);
    }
  }
  async selectPhoto(item) {
    this.setState({ 
      selectedPhotoUri: item.node.image.uri,
    })
    const base64image = await RNFS.readFile(item.node.image.uri, 'base64');
    console.log(base64image);
    this.props.screenProps.setPhoto(`data:image/png;base64,${base64image}`);
  }
  renderPhotos() {
    return this.state.photos.map((item, i) => {
      return (
        <TouchableWithoutFeedback
          key={i}
          onPress={() => this.selectPhoto(item)}>
          <Image
            style={{
              width: c.WIDTH / 4,
              height: c.WIDTH / 4,
              opacity: this.state.selectedPhotoUri ===
                item.node.image.uri ? 0.3 : 1
            }}
            source={{ uri: item.node.image.uri }}
          />
        </TouchableWithoutFeedback>
      )
    });
  }
  renderSelectedPhoto() {
    if (this.state.selectedPhotoUri) {
      return <Image
      style={{width: c.WIDTH, height: c.WIDTH}}
      source={{ uri: this.state.selectedPhotoUri }}
    />
    } else return null;
  }
  render() {
    console.log(this.state);
    return (
      <ScrollView>
        <View style={styles.container}>
          {this.renderSelectedPhoto()}
          {this.renderPhotos()}
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexWrap: 'wrap',
    flexDirection: 'row'
  },
  button: {
    marginHorizontal: c.rem,
  },
  cancelText: {
    color: c.GREY
  },
  forwardText: {
    color: 'blue'
  },
});
