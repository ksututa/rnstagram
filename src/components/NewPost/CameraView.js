import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import c from '../../lib/const';
import { withNavigationFocus } from 'react-navigation' 

// import { Camera } from 'expo';

class CameraView extends React.Component {
  constructor() {
    super();
    this.takePicture = this.takePicture.bind(this);
  }
  async takePicture () {
    const options = { base64: true }
    const result = await this.camera.takePictureAsync(options);
    const base64image = result.base64;
    console.log(base64image)
    this.props.screenProps.setPhoto(`data:image/png;base64,${base64image}`);
    this.props.navigation.navigate('PostInfo');
  }
  render() {
    console.log(this.props);
    const { isFocused } = this.props
    return (
      <View style={styles.container}>
        { isFocused ? <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          captureAudio={false}
          style={styles.camera}
        >
          <View style={styles.capture}>
            <TouchableOpacity
              onPress={this.takePicture}
              style={styles.button}
            />
          </View>
        </RNCamera> : null }
      </View>
    );
  }
}
export default withNavigationFocus(CameraView)
const styles = StyleSheet.create({
  camera: {
    width: c.WIDTH,
    height: c.HEIGHT,
    flex: 1,
    justifyContent: 'flex-end'
  },
  capture: {
    height: 200,
    width: c.WIDTH,
    backgroundColor: c.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    height: 4 * c.rem,
    width: 4 * c.rem,
    borderRadius: 2 * c.rem,
    borderWidth: 0.5 * c.rem,
    borderColor: c.GREY
  },
  container: {
    flex: 1,
  }
});