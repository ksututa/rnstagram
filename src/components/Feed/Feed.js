import React from 'react';
import Post from './Post';
import Header from './Header';
import post from '../../lib/post'
import axios from 'axios';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  AsyncStorage,
} from 'react-native';

export default class Feed extends React.Component {
  constructor() {
    super();
    this.state = {
      posts: [],
      loading: false,
      error: '',
      page: 1,
      limit: 5,
    }
    this.renderPosts = this.renderPosts.bind(this);
    // this.renderPost = this.renderPost.bind(this);
    this.loadMorePosts = this.loadMorePosts.bind(this);
    this.refresh = this.refresh.bind(this);
  }
  async componentDidMount() {
    await this.getPosts(this.state.page, this.state.limit);
  }
  async getPosts(page, limit) {
    try {
      // начало загрузки
      this.setState({ loading: true, error: '' });
      const token = await AsyncStorage.getItem('userToken');
      const email = await AsyncStorage.getItem('userEmail');
      // alert(token);
      const response = await axios
        // .get(`http://10.0.2.2:3000/440/posts?_page=${page}&_limit=${limit}`,
        .get(`http://10.0.2.2:3000/440/posts?user.name=${email}`,
        {
          headers: {
            Authorization: 'Bearer ' + token
          }
        })
      // http://localhost:3000/posts
      console.log(response);
      this.setState({     // обработка положительного сценария
        posts: this.state.posts.concat(response.data),
        loading: false,
        page,
        limit,
      })
    } catch (e) {
      console.log(e);
      this.setState({ // обработка ошибки
        loading: false,
        error: JSON.stringify(e, null, '\t')
      })
      if (e.response.status === 401) {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
      }
    }
  }
  async loadMorePosts() {
    await this.getPosts(this.state.page + 1, this.state.limit);
  }
  async refresh() {
    this.setState({ posts: [] })
    await this.getPosts(1, 5);
  }
  renderPosts(posts) {
    return posts.map((item, i) => {
      return <Post post={item} key={i} />
    });
  }
  // renderPost(post) {
  //   return <Post post={post} />
  // }
  // renderPosts = (posts) => (
  //   posts.map((item, i) => <Post post={item} key={i}/>)
  // )
  render() {
    console.log(this.state);
    const view =
      this.state.loading ? <Text>Загрузка..</Text> :
        this.state.error ? <Text>{this.state.error}</Text> :
          // this.state.posts.length ?
          //   <Text>
          //     {JSON.stringify(this.state.posts[0], null, '\t')}
          //   </Text> : null
          this.renderPosts(this.state.posts);
    // return (
    //   <ScrollView>
    //     {view}
    //   </ScrollView>
    // );
    return (
      <View>
        <Header />
        <FlatList
          data={this.state.posts}
          renderItem={({ item }) => <Post post={item} />}
          keyExtractor={item => item.id.toString()}
          // onEndReached={this.loadMorePosts}
          onRefresh={this.refresh}
          refreshing={this.state.loading}
        />
      </View>
    );
  }
}