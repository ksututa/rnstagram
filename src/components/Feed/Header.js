import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import c from '../../lib/const';

export default Header = () => (
  <View style={styles.container}>
    <Text style={styles.text}>RNstagram</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    paddingTop: c.rem,
    height: 4 * c.rem,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: c.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: c.GREY,
  },
  text: {
    fontSize: 2 * c.rem,
    fontFamily: 'Billabong',
    color: c.BLACK,
  },
});