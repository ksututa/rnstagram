import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
} from 'react-native';
import c from '../../lib/const';
import Author from './Author';
import PostImage from './PostImage';
import OptionsBar from './OptionsBar';
import Comment from './Comment'

export default class Post extends React.Component {
  constructor(){
    super();
    this.state = {
      commentsExpanded: false,
    }
    this.handleExpand = this.handleExpand.bind(this);
    this.toggleExpand = this.toggleExpand.bind(this);
  }
  handleExpand() {
    this.setState({ commentsExpanded: true })
  }
  toggleExpand() {
    this.setState({ commentsExpanded: !this.state.commentsExpanded })
  }
  renderComments = (comments) => {
    if (comments.length) {
      if (this.state.commentsExpanded) {
        return comments.map((item, i) => (
          <Comment comment={item} key={i} />
        ))
      } else {
        return (
          <View>
            <Comment
              comment={comments[0]}
              numberOfLines={2}
            />
          </View>
        )
      }
    } else {
      return null;
    } 
  }
  render() {
    const post = this.props.post
    return(
      <View style={styles.container}>
        <Author
          name={post.user.name}
          avatar={post.user.avatar}
          />
        <PostImage photo={post.photo}/>
        <OptionsBar />
        <View style={styles.comments}>
          {this.renderComments(post.comments)}
          <TouchableOpacity
            onPress={this.toggleExpand}
            >
            <Text style={styles.expandText}>
              {this.state.commentsExpanded ? 'скрыть' : 'ещё'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    paddingBottom: 0.5 * c.rem,
  },
  comments: {
    paddingHorizontal: 0.7 * c.rem
  },
  expandText: {
    fontSize: 0.8 * c.rem,
    color: c.GREY,
    textDecorationLine: 'underline'
  }
});