import React from 'react';
import {
  TextInput,
  StyleSheet
} from 'react-native';
import c from '../../lib/const';

export default CustomInput = (props) => {
  console.log('custom input', props);
  return (
    <TextInput
      {...props}
      style={[styles.input, props.style]}
    />
  );
}
const styles = StyleSheet.create({
  input: {
    height: 3 * c.rem,
    width: '80%',
    borderColor: c.BLACK,
    borderWidth: 1,
    borderRadius: 0.2 * c.rem,
    paddingHorizontal: c.rem,
  },
});