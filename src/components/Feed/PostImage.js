import React from 'react';
import {
  Image,
} from 'react-native';
import c from '../../lib/const';

export default PostImage = (props) => (
  <Image
    style={{ width: c.WIDTH, height: c.WIDTH }}
    source={{ uri: props.photo }}
  />
)