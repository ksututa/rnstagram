import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import c from '../../lib/const';
export default Comment = (props) => {
  return (
      <Text
        numberOfLines={props.numberOfLines}
        style={styles.commentText}>
        <Text style={styles.nameText}>{props.comment.user.name} </Text>
        {props.comment.text}
      </Text>
  );
  
}
const styles = StyleSheet.create({
  commentText: {
    color: c.BLACK,
  },
  nameText: {
    color: c.BLACK,
    fontWeight: 'bold',
  },
});