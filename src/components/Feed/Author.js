import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import c from '../../lib/const';

export default Author = (props) => {
  const image = props.avatar ?
    <Image
      style={styles.avatar}
      source={{ uri: props.avatar }}
    /> : <View style={styles.avatar} />
  return (
    <View style={styles.container}>
        {image}
      <Text style={styles.name}>
        {props.name}
      </Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    height: 4 * c.rem,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  avatar: {
    height: 2.5 * c.rem,
    width: 2.5 * c.rem,
    borderRadius: 1.3 * c.rem,
    margin: 0.7 * c.rem,
  },
  name: {
    fontSize: c.rem,
    color: c.BLACK
  },
});