import React from 'react';
import {
  StyleSheet,
  View
} from 'react-native';
import c from '../../lib/const';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default OptionsBar = () => {
  return (
    <View style={styles.optionsPane}>
      <Icon name='heart' size={2 * c.rem} style={styles.icon} />
      <Icon name='comment' size={2 * c.rem} style={styles.icon} />
    </View>
  );

}
const styles = StyleSheet.create({
  optionsPane: {
    height: 3 * c.rem,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  icon: {
    marginLeft: 0.7 * c.rem,
    color: c.BLACK,
  },
});