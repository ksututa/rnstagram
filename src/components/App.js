import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Feed from './Feed/Feed';
import NewPost from './NewPost/NewPost';
import User from './User/User';
import Settings from './User/Settings';
import PhotoPicker from './NewPost/PhotoPicker';
import PostInfo from './NewPost/PostInfo';
import CameraView from './NewPost/CameraView';
import Register from './Auth/Register';
import Auth from './Auth/Auth';
import Splash from './Auth/Splash';
import c from '../lib/const';
import {
  createAppContainer,
  createBottomTabNavigator,
  createStackNavigator,
  createSwitchNavigator,
  createDrawerNavigator,
} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

const PhotoPickerTab = createBottomTabNavigator({
  PhotoPicker: PhotoPicker,
  CameraView: CameraView,
}, {
  defaultNavigationOptions: ({ navigation })=>({
    tabBarLabel: () => {
      const { routeName } = navigation.state;
      let label = routeName;
      if (routeName === 'PhotoPicker') {
        label = 'Библиотека';
      } else if (routeName === 'CameraView') {
        label = 'Фото';
      }
      return (<Text style={{ textAlign: 'center' }}>{label}</Text>);
    }
  }),
});
const UserDrawer = createDrawerNavigator({
  User: User,
  Settings: Settings,
}, {
  drawerPosition: 'right'
})
const MainTab = createBottomTabNavigator({
  Feed: Feed,
  NewPost: NewPost,
  User: UserDrawer,
}, {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarOnPress: ({ defaultHandler }) => {
        if (navigation.state.routeName === 'NewPost') {
          navigation.navigate('PhotoPickerTab');
        } else {
          defaultHandler();
        }
      },
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let icon;
        if (routeName === 'Feed') {
          icon = <Icon color={tintColor} name={'ios-home'} size={30} />
        } else if (routeName === 'NewPost') {
          icon = <Icon color={tintColor} name={'ios-add-circle-outline'} size={30} />
        } else if (routeName === 'User') {
          icon = <Icon color={tintColor} name={'ios-contact'} size={30} />
        }
        return icon;
      }
    }),
    tabBarOptions: {
      activeTintColor: c.BLACK,
      inactiveTintColor: c.GREY,
      showLabel: false,
    },
    swipeEnabled: true,
    initialRouteName: 'Feed'
  })

const AppStack = createStackNavigator({
  MainTab: MainTab,
  PhotoPickerTab,
  PostInfo: PostInfo,
}, {
  defaultNavigationOptions: ({navigation}) => ({
    header: (navigation.state.routeName === 'MainTab' ||
    navigation.state.routeName === 'PostInfo') && null,
    headerStyle: {
      height: 3 * c.rem,
    },
    headerLeft: (
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Feed')}>
        <Text style={styles.cancelText}>Отмена</Text>
      </TouchableOpacity>
    ),
    headerRight: (
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('PostInfo')}>
        <Text style={styles.forwardText}>Далее</Text>
      </TouchableOpacity>
    ),
  })
});
const AuthStack = createStackNavigator({
  Auth: Auth,
  Register: Register,
})
const AuthSwitch = createSwitchNavigator({
  Splash: Splash,
  AuthStack: AuthStack,
  AppStack: AppStack,
})
const AppContainer = createAppContainer(AuthSwitch)

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      photo: '',
    }
    this.setPhoto = this.setPhoto.bind(this);
  }
  setPhoto(photo) {
    this.setState({ photo });
  }
  render() {
    return (
      <View style={styles.container}>
        <AppContainer 
          screenProps={{
            photo: this.state.photo,
            setPhoto: this.setPhoto,
          }}
          />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    marginHorizontal: c.rem,
  },
  cancelText: {
    color: c.GREY
  },
  forwardText: {
    color: 'blue'
  },
});
