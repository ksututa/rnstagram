import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  AsyncStorage,
} from 'react-native';
export default class User extends React.Component {
  constructor() {
    super();
    this.clearSession = this.clearSession.bind(this);
  }
  async clearSession() {
    await AsyncStorage.clear();
    this.props.navigation.navigate('AuthStack');
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Settings</Text>
        <Button title={'Выйти'} onPress={this.clearSession}/>
        <Button
          title={'open'}
          onPress={() => this.props.navigation.openDrawer()}
          />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});