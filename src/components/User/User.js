import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  AsyncStorage,
} from 'react-native';
export default class User extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>User</Text>
        <Button
          title={'open'}
          onPress={() => this.props.navigation.openDrawer()}
          />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});