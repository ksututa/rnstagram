export default post = {
  photo: 'http://lorempixel.com/640/480/cats',
  id: '1',
  user: {
    name: 'just_a_boy',
    id: '2',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/tjrus/128.jpg'
  },
  likes: 17,
  comments: [
    {
      text: 'Such a lovely cat!',
      user: {
        name: 'just_a_girl',
        id: '3',
        avatar: '///'
      }
    }
  ]
}