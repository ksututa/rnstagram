module.exports = function() {
    var faker = require('faker');
    var _ = require('lodash');
    return {
        posts: _.times(100, function(n) {
            return {
                user: {
                    id: faker.random.number(10),
                    name: faker.name.findName(),
                    avatar: faker.internet.avatar(),
                },
                id: n,
                photo: faker.image.image(),
                likes: faker.random.number(100),
                comments: [
                    {
                        id: 1,
                        text: faker.lorem.text(),
                        likes: faker.random.number(10),
                        user: {
                            id: faker.random.number(10),
                            name: faker.name.findName(),
                            avatar: faker.internet.avatar(),
                        },
                    },
                    {
                        id: 2,
                        text: faker.lorem.text(),
                        likes: faker.random.number(10),
                        user: {
                            id: faker.random.number(10),
                            name: faker.name.findName(),
                            avatar: faker.internet.avatar(),
                        },
                    },
                    {
                        id: 3,
                        text: faker.lorem.text(),
                        likes: faker.random.number(10),
                        user: {
                            id: faker.random.number(10),
                            name: faker.name.findName(),
                            avatar: faker.internet.avatar(),
                        },
                    },
                ],
            }
        }),
        users: [],
    }
}