import {
  Dimensions
} from 'react-native'

const {width, height} = Dimensions.get('screen');
console.log(width, height);
const min = width < height ? width : height;

const rem = (side) => {
  return side > 310
    ? side > 350
      ? side > 390
        ? side > 470
          ? side > 530
            ? side > 590
              ? side > 790
                ? side > 990
                  ? 36
                  : 30
                : 22
              : 22
            : 22
          : 22
        : 15
      : 15
    : 12;
};

const c = {
  WIDTH: width,
  HEIGHT: height,
  rem: rem(min),
  WHITE: '#ffffff',
  BLACK: '#000000',
  GREY: '#BBB'
}
 export default c;